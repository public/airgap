VERSION := development
GIT_REF := $(shell git log -1 --format=%H)
GIT_AUTHOR := $(shell git log -1 --format=%an)
GIT_PUBKEY := $(shell git log -1 --format=%GK)
GIT_TIMESTAMP := $(shell git log -1 --format=%cd --date=iso)
EFI := false
,:=,
export

## Use env vars from latest release when reproducing
ifdef REPRODUCE
	include dist/release.env
	export
endif

## Prevents use of caching when building docker image
ifdef NOCACHE
	NO_CACHE := --no-cache
endif

.DEFAULT_GOAL :=
.PHONY: default
default: \
	out/release.env \
	out/manifest.txt \
	out/airgap.iso

## Primary targets

out/airgap.iso: Containerfile $(shell git ls-files rootfs)
	SOURCE_DATE_EPOCH=1 \
	docker build \
		--progress=plain \
		--output type=local,rewrite-timestamp=true,dest=out \
		--build-arg SOURCE_DATE_EPOCH=1 \
		--build-arg VERSION="$(VERSION)" \
		--build-arg GIT_REF="$(GIT_REF)" \
		--build-arg GIT_AUTHOR="$(GIT_AUTHOR)" \
		--build-arg GIT_PUBKEY="$(GIT_PUBKEY)" \
		--build-arg GIT_TIMESTAMP="$(GIT_TIMESTAMP)" \
		$(NO_CACHE) \
		-f Containerfile \
		.

## Development Targets

out/dev-shell.digest: Containerfile | out
	docker build --target dev -f Containerfile -q . > $@

.PHONY: shell
shell: out/dev-shell.digest
	docker run -it $(shell cat $<) /bin/sh

.PHONY: vm
vm: out/dev-shell.digest out/airgap.iso out/sdcard.img
	docker run -it -v ./out:/out $(shell cat $<) sh -c "\
		swtpm socket \
			--tpmstate dir=. \
			--ctrl type=unixio,path=vtpm-sock \
			--tpm2 & \
		qemu-system-x86_64 \
			-m 4G \
			-machine pc \
			-chardev socket,id=chrtpm,path=vtpm-sock \
			-usb -device canokey,file=/out/canokey-file \
			-tpmdev emulator,id=tpm0,chardev=chrtpm \
			-device tpm-tis,tpmdev=tpm0 \
			-usb \
			-device sdhci-pci \
			-device sd-card,drive=external \
			-drive id=external,if=none,format=raw,file=out/sdcard.img \
			-device usb-storage,drive=usbdrive \
			$(if $(filter $(EFI),true) ,\
				-bios /usr/share/ovmf/OVMF.fd \
				-drive id=boot$(,)if=virtio$(,)format=raw$(,)file=out/airgap.iso \
			,\
				-drive id=usbdrive,if=none,format=raw,file=out/airgap.iso \
				-boot order=c \
			) \
			$(if (,$(wildcard /dev/kvm)),,-cpu host --accel kvm) \
			-nographic; \
		"

## Release, Signing, Verification, and Reproduction Targets

.PHONY: clean
clean:
	rm -rf out

.PHONY: update
update:
	python3 src/update.py

.PHONY: release
release: clean
	$(MAKE) NOCACHE=1 VERSION=$(VERSION)
	rm -rf dist/*
	cp -R out/release.env out/airgap.iso out/manifest.txt dist/

.PHONY: sign
sign:
	set -e; \
	git config --get user.signingkey 2>&1 >/dev/null || { \
		echo "Error: git user.signingkey is not defined"; \
		exit 1; \
	}; \
	fingerprint=$$(\
		git config --get user.signingkey \
		| sed 's/.*\([A-Z0-9]\{16\}\).*/\1/g' \
	); \
	gpg --armor \
		--detach-sig  \
		--output dist/manifest.$${fingerprint}.asc \
		dist/manifest.txt

.PHONY: verify
verify: | dist/manifest.txt
	set -e; \
	for file in dist/manifest.*.asc; do \
		echo "\nVerifying: $${file}\n"; \
		gpg --verify $${file} dist/manifest.txt; \
	done;

.PHONY: reproduce
reproduce: clean | out
	$(MAKE) REPRODUCE=true NOCACHE=1
	diff -q out/manifest.txt dist/manifest.txt;

out:
	mkdir -p $@

out/release.env: $(shell git ls-files) | out
	echo 'VERSION=$(VERSION)'              > out/release.env
	echo 'GIT_REF=$(GIT_REF)'             >> out/release.env
	echo 'GIT_AUTHOR=$(GIT_AUTHOR)'       >> out/release.env
	echo 'GIT_PUBKEY=$(GIT_PUBKEY)'       >> out/release.env
	echo 'GIT_TIMESTAMP=$(GIT_TIMESTAMP)' >> out/release.env

out/manifest.txt: out/airgap.iso out/release.env | out
	openssl sha256 -r \
		out/airgap.iso \
		out/release.env \
	| sed -e 's/ \*out\// /g' -e 's/ \.\// /g' \
	> $@
