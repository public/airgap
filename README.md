# AirgapOS #

<https://git.distrust.co/public/airgap>

## About ##

A full-source-bootstrapped, deterministic, minimal, immutable, and offline,
workstation linux distribution designed for creating and managing secrets
offline.

Built for those of us that want to be -really- sure our most important secrets
are managed in a clean environment with an "air gap" between us and the
internet with high integrity on the supply chain of the firmware and OS used.

## Uses ##
 * Generate PGP keychain
 * Store/Restore gpg keychain to security token such as a Yubikey or Nitrokey
 * Signing cryptocurrency transactions
 * Generate/backup BIP39 universal cryptocurrency wallet seed
 * Store/Restore BIP39 seed to a hardware wallet such as a Trezor or Ledger

## Features ##
 * Deterministic iso generation for multi-party code->binary verification
 * Small footprint (< 100MB)
 * Immutable and Diskless: runs from initramfs
 * Network support and most drivers removed to minimize exfiltration vectors

## Requirements ##

### Software ###

* docker 26+

### Hardware ###

* x86_64 PC or laptop
    * linuxboot/heads firmware supported and recommended for multi-use machine
      * Allows for signed builds, and verification of signed sd card payloads
    * Ensure any Wifi/Disk/Bluetooth/Audio devices are disabled/removed
* Blank flash drive
* Blank SD card

## Build ##

### Update git submodules

```
git submodule update --init --recursive
```

### Build a new release

```
make release
```

### Reproduce an existing release

```
make reproduce 
```

### Sign an existing release

```
make sign
```

## Provisioning ##

1. Write airgap.iso to CD-ROM or SD Card
  a. `dd if=out/airgap.iso of=/dev/sda bs=1M conv=sync status=progress`
  b. `cdrecord out/airgap.iso`

2. Verify media still produces expected hash
  ```
  sha256sum out/airgap.iso
  head -c $(stat -c '%s' airgap.iso) /dev/sda | sha256sum
  ```

## Setup ##

Assumes target is running Pureboot or Coreboot/heads

1. Boot to shell: ```Options -> Recovery Shell```
2. Mount SD card
	```
	mount-usb
	mount -o remount,rw /media
	```
3. Insert chosen GPG Smartcard device
4. Initialize smartcard
	```
	gpg --card-status
	```
5. Sign target iso
	```
	cd /media
	gpg --armor --detach-sign airgap.iso
	```
6. Unmount
	```
	cd
	umount /media
	sync
	```
7. Reboot

## Usage ##

1. Insert remote attestation device
2. Power on, and verify successful remote attestation
3. Boot to airgap via: Options -> Boot Options -> USB Boot

## Development ##

### Build develop image
```
make
```

### Boot image in qemu
```
make vm
```

### Enter shell in build environment
```
make shell
```

## Hardware Compatibility ##

### Tested Models

* Purism Librem 14

* HP 13" Intel Celeron - 4GB Memory - 64GB eMMC, HP 14-dq0052dx, SKU: 6499749, UPC: 196548430192, DCS: 6.768.5321, ~USD $179.99

* Lenovo 14" Flex 5i FHD Touchscreen 2-in-1 Laptop - Intel Core i3-1215U - 8GB Memory - Intel UHD Graphics, SKU: 6571565, ~USD $379.99

### Disabling Secure Boot 

AirgapOS can't be booted using secure boot. Therefore it has to be disabled. Alternative systems like Heads may be used.

#### Instructions to Disable Secure Boot in BIOS

1. Restart your computer

2. **Enter BIOS/UEFI Setup**:
   - As your computer starts up, press the appropriate key to enter the BIOS/UEFI setup. Common keys include:
     - **F2** (Dell, Acer, Lenovo)
     - **Delete** (ASUS, MSI)
     - **F10** (HP)
     - **Esc** (Some systems)
   - You may see a prompt on the screen indicating which key to press

3. **Navigate to the Secure Boot Option**:
   - Once in the BIOS/UEFI setup, use the arrow keys to navigate through the menus. Look for a tab or section labeled **"Boot," "Security,"** or **"Authentication."**
   - The exact location of the Secure Boot option can vary, so you may need to explore a bit

4. **Locate Secure Boot**:
   - Find the **Secure Boot** option within the selected menu. It may be listed as **"Secure Boot Control"** or simply **"Secure Boot."**

5. **Disable Secure Boot**:
   - Select the Secure Boot option and change its setting to **Disabled**. This is usually done by pressing **Enter** and then selecting **Disabled** from the options.

6. **Save Changes and Exit**:
   - After disabling Secure Boot, navigate to the **Exit** tab or section.
   - Choose the option to **Save Changes and Exit**. Confirm any prompts that appear to save your changes.

7. **Reboot Your Computer**:
   - Your computer will restart. Secure Boot should now be disabled.